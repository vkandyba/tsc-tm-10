package ru.vkandyba.tm.api.controller;

public interface ICommandController {

    void exit();

    void showInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showErrorArgument();

    void showErrorCommand();

}
