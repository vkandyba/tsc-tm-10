package ru.vkandyba.tm.api;

import ru.vkandyba.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
