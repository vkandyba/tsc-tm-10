package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
