package ru.vkandyba.tm.repository;

import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) { projects.add(project); }

    @Override
    public void remove(Project project) { projects.remove(project); }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

}
